//alert('hola a todos');


function addEventToButtons (){
    
    const buttons = document.querySelectorAll('.comment-like');
    
    buttons.forEach(function(currentButton){
        currentButton.addEventListener('click', function() {
            
            const icon = currentButton.querySelector('i');
            const isActive = icon.classList.contains('fas');
            
            currentButton.classList.toggle('comment-like-active');
            
            if(isActive){
                
                icon.classList.replace('fas','far');
                currentButton.style.color = 'black';
            }
            else{
                icon.classList.replace('far','fas');
                currentButton.style.color = 'red';
                
            }
            //console.log('click en el boton')
        });
    });
};

const option = document.querySelector('#options');
const postButton = document.querySelector('#post-button');
const commentInput = document.querySelector('#comment-input');
const commentFeed = document.querySelector('#comment-feed');

addEventToButtons();

postButton.addEventListener('click', function() {
    
    const commentContainer = document.createElement('div');
    commentContainer.className = 'comment';
    
    const commentImgContainer = document.createElement('div');
    commentImgContainer.className = 'comment-img-container';
    
    const userAvatar = document.createElement('img');
    userAvatar.className = 'user-avatar';
    userAvatar.setAttribute('src', 'https://scontent-mia3-2.cdninstagram.com/v/t51.2885-19/s150x150/22582088_735399073336517_7044282068120895488_n.jpg?_nc_ht=scontent-mia3-2.cdninstagram.com&_nc_ohc=Ws7h4whnUQgAX_p0RMd&oh=d3307fe00d1fe04244523abbf2e8adea&oe=5E897729')
    // aqui termina el area de la imagen
    
    const commentInfoContainer = document.createElement('div');
    commentInfoContainer.className ='comment-info-container';
    
    const infoPharagraph = document.createElement('p');
    infoPharagraph.className = 'comment-content';

    const userComment = document.createElement('a');
    userComment.className = 'user-page';

    const userName = document.createElement('b');
    userName.className = 'user-name';
    userName.innerText = 'colossal';

    const likeButton = document.createElement('button');
    likeButton.className = 'comment-like';

    const heartIcon = document.createElement('i');
    heartIcon.className = 'far fa-heart';
    
    const userInput = document.createTextNode(commentInput.value);

    commentContainer.appendChild(commentImgContainer);
    commentImgContainer.appendChild(userAvatar);
    
    commentContainer.appendChild(commentInfoContainer);
    commentInfoContainer.appendChild(infoPharagraph);
    infoPharagraph.appendChild(userComment);
    userComment.appendChild(userName);
    infoPharagraph.appendChild(userInput);
    infoPharagraph.appendChild(likeButton);
    likeButton.appendChild(heartIcon);
    
    commentFeed.appendChild(commentContainer);
    
    
    commentInput.value = '';
    addEventToButtons();
    
});

addEventToButtons();

option.addEventListener('click', function (){
    const mod = document.querySelector('.modal');
    mod.style.display = "flex";
});



